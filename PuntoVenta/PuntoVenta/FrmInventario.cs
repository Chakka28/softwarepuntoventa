﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PuntoVentaBOL;
using PuntoVentaEntities;


namespace PuntoVenta
{
    public partial class FrmInventario : Form
    {

        internal User Usuario { get; set; }
        private Producto eproducto;
        private ProviderBOL providerbol;
        private CategoryBOL cbol;
        private ProductoBOL pbol;

        public FrmInventario()
        {
            InitializeComponent();
            this.CenterToScreen();

        }

        private void FrmInventario_Load(object sender, EventArgs e)
        {
            pbol = new ProductoBOL();
            providerbol = new ProviderBOL();
            cbol = new CategoryBOL();
            eproducto = new Producto();
            btbGuardar.Visible = false;
            btbañadirnuevo.Visible = false;
            dginventario.ReadOnly = true;

            ///Estas lineas son para el background worker
            //bwCargarDatos.RunWorkerAsync();
            //CheckForIllegalCrossThreadCalls = false;
            MostrarDatos();
        }

        /// <summary>
        /// Este metodo se va a encargar de habilitar las diferentes opciones
        /// que están disponibles a nivel de usuario
        /// varian entre los roles administrador y cajero
        /// </summary>
        /// <param name="usuario"></param>
        private void VerificarUsario(User usuario)
        {
            if (usuario.Rol.Equals("cajero"))
            {
                //btbGuardar.Visible = false;
                //btbañadirnuevo.Visible = false;
                //btbEliminar.Visible = false;
                //btbGuardar.Visible = false;
                //btbModificar.Visible = false;
                //btNuevo.Visible = false;
            }
            else
            {
                //btbmodificar.visible = true;
                //btnuevo.visible = true;
                //btbeliminar.visible = true;
                //btbguardar.visible = false;
                //btbañadirnuevo.visible = false;


            }
        }

        private void FrmInventario_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (Owner != null)
            {
                Owner.Show();
            }
        }

        private void bwCargarDatos_DoWork(object sender, DoWorkEventArgs e)
        {
            MostrarDatos();
        }

        /// <summary>
        /// Agrega los registros de la base de datos en el
        /// datagridview.
        /// </summary>
        private void MostrarDatos()
        {
            List<Producto> p = pbol.CargarProductos();

            dginventario.DataSource = p;
                  
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {

        }

        private void btnuevoPro_Click(object sender, EventArgs e)
        {

        }

        private void btbGuardar_Click(object sender, EventArgs e)
        {
            try {

                eproducto = new Producto();

                eproducto.categoria = (Categoria) cbCategorias.SelectedItem;
                eproducto.provedor = (Provider)cbproveedores.SelectedItem;
                eproducto.codigo = txtCodigo.ToString().Trim();
                eproducto.cantidad = Convert.ToInt32(txtstock.ToString().Trim());
                eproducto.precio = Convert.ToDecimal(txtPrecio.ToString().Trim());
                eproducto.Descripcion = rtxdescripcion.ToString().Trim();

            }
            catch (Exception ex) {

                MessageBox.Show((ex.Message));
            }
        }

        private void btbañadirnuevo_Click(object sender, EventArgs e)
        {

        }

        private void btNuevo_Click(object sender, EventArgs e)
        {
            btbGuardar.Visible = false;
            btbañadirnuevo.Visible = true;
            Limpiar();
            cbproveedores.DataSource = providerbol.CargarTodosProveedores();
            cbCategorias.DataSource = cbol.CargarTodas();
        }

        private void btbModificar_Click(object sender, EventArgs e)
        {
            
            if (eproducto.id > 0)
            { 
                btbGuardar.Visible = true;
                btbañadirnuevo.Visible = false;
                cbproveedores.DataSource = providerbol.CargarTodosProveedores();
                cbCategorias.DataSource = cbol.CargarTodas(); 
                ///se llama el mètodo para modificar los datos
            }
            else {
                MessageBox.Show("Debe seleccionar un producto antes de modificarlo");
            }

        }

        private void btbEliminar_Click(object sender, EventArgs e)
        {

        }

        private void dginventario_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btbGuardar.Visible = false;
            btbañadirnuevo.Visible = false;
            int row = dginventario.SelectedRows.Count > 0 ? dginventario.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                Limpiar();
                eproducto = new Producto();
                eproducto = (Producto)dginventario.CurrentRow.DataBoundItem;
                CargarDatos(eproducto);

            }
        }

        private void CargarDatos(Producto eproducto)
        {
            txtCodigo.Text = eproducto.codigo;
            txtPrecio.Text = eproducto.precio.ToString();
            txtstock.Text = eproducto.cantidad.ToString();
            rtxdescripcion.Text = eproducto.Descripcion;
            cbCategorias.Items.Clear();
            cbproveedores.Items.Clear();
            cbCategorias.Items.Add(eproducto.categoria);
            cbproveedores.Items.Add(eproducto.provedor);
            cbproveedores.SelectedItem = eproducto.provedor;
            cbCategorias.SelectedItem = eproducto.categoria;
            

            
        }

        private void Limpiar()
        {
            eproducto = new Producto();
            cbCategorias.DataSource = null;
            cbCategorias.Items.Clear();
            cbproveedores.DataSource = null;
            cbproveedores.Items.Clear();
            txtCodigo.Text = eproducto.codigo;
            txtPrecio.Text = eproducto.precio.ToString();
            txtstock.Text = eproducto.cantidad.ToString();
            rtxdescripcion.Text = eproducto.Descripcion;
        }
    }
}

