﻿using PuntoVentaBOL;
using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PuntoVenta
{
    public partial class SeeProviders : Form
    {
        private ProviderBOL pbol;

        public SeeProviders()
        {
            InitializeComponent();
        }

        private void SeeProviders_Load(object sender, EventArgs e)
        {
            pbol = new ProviderBOL();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dgProviders.DataSource = pbol.LoadProvider(txtFiltro.Text.Trim());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            new AddProvider() { }.ShowDialog();
            Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                Provider p = new Provider();
                p = (Provider)dgProviders.SelectedRows[0].DataBoundItem;

                Hide();
                new AddProvider() { pro = p }.ShowDialog();
                Show();
            }
            catch (Exception)
            {

                MessageBox.Show("Seleccione una columna");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Provider p = new Provider();
                p = (Provider)dgProviders.SelectedRows[0].DataBoundItem;

                DialogResult result = MessageBox.Show("¿Desea Eliminar este proveedor?",
             "Mensaje del sistema", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                
                    pbol.Delete(p);
                }
                else
                {

                }
            }
            catch (Exception)
            {
                MessageBox.Show("Seleccione una columna");
            }


        }
    }
}
