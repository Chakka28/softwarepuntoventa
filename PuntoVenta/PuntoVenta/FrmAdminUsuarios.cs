﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PuntoVentaEntities;
using PuntoVentaBOL;

namespace PuntoVenta
{
    public partial class FrmAdminUsuarios : Form
    {

        //internal User Usuario { get; set; }
        private User euser;
        private UserBOL ubol;

        public FrmAdminUsuarios()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void FrmAdminUsuarioscs_Load(object sender, EventArgs e)
        {
            btbCrear.Visible = false;
            btbGuardar.Visible = false;
            euser = new User();
            ubol = new UserBOL();
            dgvUsers.DataSource = ubol.CargarDatos();
        }

        private void btbAgregar_Click(object sender, EventArgs e)
        {
            btbCrear.Visible = true;
            btbGuardar.Visible = false;
            euser = new User();
            txtNombre.Text = euser.name;
            txtcedula.Text = euser.cedula;
            txtusername.Text = euser.userid;
            txtPass.Text = euser.pass;
            cbActivo.Visible = false;
            txtPass.Visible = true;
            lblPass.Visible = true;
        }
        //
        private void btbModificar_Click(object sender, EventArgs e)
        {
            btbCrear.Visible = false;
            btbGuardar.Visible = true;
            cbActivo.Visible = true;
            txtPass.Visible = false;
            lblPass.Visible = false;
        }

        private void btbEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                int row = dgvUsers.SelectedRows.Count > 0 ? dgvUsers.SelectedRows[0].Index : -1;
                if (row >= 0)
                {
                    

                    euser = (User)dgvUsers.CurrentRow.DataBoundItem;

                    DialogResult result = MessageBox.Show("¿Desea Eliminar este usuario?",
                "Mensaje del sistema", MessageBoxButtons.YesNo);

                    if (result == DialogResult.Yes)
                    {
                        if (ubol.EliminarUsuario(euser))
                        {
                            Limpiar();
                            dgvUsers.DataSource = ubol.CargarDatos();
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        
        }

        private void btbCrear_Click(object sender, EventArgs e)
        {
            try {

                euser.name = txtNombre.Text.Trim();
                euser.cedula = txtcedula.Text.Trim();
                euser.userid = txtusername.Text.Trim();
                euser.Rol = cbRol.Text;
                euser.pass = txtPass.Text.Trim();
                euser.active = true;
                string repass = txtrepass.Text.Trim();

                if (ubol.InsertarDatos(euser, repass))
                {
                    Limpiar();
                    MessageBox.Show("Registro realizado con éxito", "Mensaje del sistema");
                    dgvUsers.DataSource = ubol.CargarDatos();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Este metodo se encarga de limpiar los textbox 
        /// cada que vez que termina una transacción
        /// </summary>
        private void Limpiar()
        {
            euser = new User();
            txtNombre.Text = euser.name;
            txtcedula.Text = euser.cedula;
            txtusername.Text = euser.userid;
            txtPass.Text = euser.pass;
            txtrepass.Text = " ";
            cbActivo.Visible = false;
        }

        private void btbGuardar_Click(object sender, EventArgs e)
        {
            try
            {

                euser.name = txtNombre.Text.Trim();
                euser.cedula = txtcedula.Text.Trim();
                euser.userid = txtusername.Text.Trim();
                euser.Rol = cbRol.Text;
                euser.pass = txtPass.Text.Trim();
                
            
                if (ubol.ModificarDatos(euser))
                {
                    Limpiar();
                    MessageBox.Show("Actualización realizada con éxito", "Mensaje del sistema");
                    dgvUsers.DataSource = ubol.CargarDatos();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void FrmAdminUsuarios_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("Perdera los cambios que no sean guardados ¿Desea salir?",
                 "Mensaje del sistema", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                if (Owner != null)
                {
                    Owner.Show();
                }

            }
            else
            {
                e.Cancel = true;
            }
        }

        private void dgvUsers_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dgvUsers.SelectedRows.Count > 0 ? dgvUsers.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                Limpiar();
                euser = new User();
                cbActivo.Visible = true;
                cbActivo.Checked = true;
                euser = (User)dgvUsers.CurrentRow.DataBoundItem;
                btbCrear.Visible = false;
                btbGuardar.Visible = false;
                CargarDatos(euser);

            }
        }

        private void CargarDatos(User euser)
        {
            txtNombre.Text = euser.name;
            txtcedula.Text = euser.cedula;
            txtusername.Text = euser.userid;
            cbRol.Text = euser.Rol;
            txtPass.Visible = false;
            lblPass.Visible = false;
            lblrepass.Visible = false;
            txtrepass.Visible = false;
        }
    }
}
