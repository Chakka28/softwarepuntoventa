﻿using PuntoVentaBOL;
using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PuntoVenta
{
    public partial class AddProvider : Form
    {
        ProviderBOL pbol;
        public Provider pro { get; set; }

        public AddProvider()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Manda a guardar los datos del proevedor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Provider p = new Provider
                {
                    id = pro == null ? 0 : pro.id,
                    company = txtEmpresa.Text.Trim(),
                    telephone = txtTelefono.Text.Trim()
                };
                pbol.insert(p);
                MessageBox.Show("Datos agregados");
                this.Close();
            }
            catch (Exception ex )
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// solo deja ingresar numeros en el txt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar == (char)Keys.Back)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void AddProvider_Load(object sender, EventArgs e)
        {
            pbol = new ProviderBOL();
            if (pro?.id > 0)
            {
                txtEmpresa.Text = pro.company;
                txtTelefono.Text = pro.telephone;

            }
        }

        private void txtTelefono_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
