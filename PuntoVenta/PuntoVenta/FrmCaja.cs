﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PuntoVentaBOL;
using PuntoVentaEntities;

namespace PuntoVenta
{
    public partial class FrmCaja : Form
    {
        public User Usuario { get; set; }
        internal User usuario { get; set; }
        private ProductoBOL pbol;
        private FacturacionBOL fbol;
        private List<Producto> productos;
        private decimal total;

        public FrmCaja()
        {
            InitializeComponent();
        }

        private void FrmCaja_FormClosing(object sender, FormClosingEventArgs e)
        {
            //TODO: Se debe de mostrar mensaje para ver si de verdad desea eliminar esa factura o no
            // también falta lo del número de comprobante por factura.
            if (Owner != null)
            {
                Owner.Show();
            }
        }

        private void FrmCaja_Load(object sender, EventArgs e)
        {
            fbol = new FacturacionBOL();
            pbol = new ProductoBOL();
            productos = new List<Producto>();
            comboBox1.SelectedItem = "CONTADO";


        }

        private void btbAgregar_Click(object sender, EventArgs e)
        {
            FrmCaja caja = new FrmCaja
            {
                usuario = usuario

            };
            caja.Show(this);

        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Producto p = new Producto();
                p = pbol.cargarProducto(txtCode.Text);
                MessageBox.Show(p.Descripcion);

                if (p.id != 0)
                {
                    if (p.categoria.id == 14)
                    {
                        try
                        {
                            string input = Microsoft.VisualBasic.Interaction.InputBox("Ingrese el peso", "Datos", "1000", 500, 300);
                            decimal cantidad = Convert.ToInt32(input);
                            p.precio = (p.precio / 1000) * cantidad;
                            reload(p);
                        }
                        catch (Exception)
                        {

                     
                        }
                        
                    }
                    else
                    {
                        reload(p);
                    }



                }
                else
                {
                    MessageBox.Show("Producto no registrado");
                }






            }
        }

        public void reload(Producto p)
        {
            if (revisarProducto(p))
            {
                total += p.precio;
                dgProducto.DataSource = null;
                dgProducto.DataSource = productos;
                lblt.Text = total.ToString();
            }
            else
            {
                productos.Add(p);
                total += p.precio;
                dgProducto.DataSource = null;
                dgProducto.DataSource = productos;
                lblt.Text = total.ToString();
            }

        }

        public Boolean revisarProducto(Producto p)
        {
            int cont = 0;
            foreach (Producto pro in productos)
            {
                if (pro.id == p.id)
                {
                    productos[cont].cantidad++;
                    return true;
                }
                cont++;
            }
            return false;
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                Producto p = new Producto();
                p = (Producto)dgProducto.SelectedRows[0].DataBoundItem;
                productos.Remove(p);
                dgProducto.DataSource = null;
                dgProducto.DataSource = productos;
                total -= p.precio;
                lblt.Text = total.ToString();
            }
            catch (Exception)
            {

                MessageBox.Show("Seleccione una columna");
            }

        }

        private void dgProducto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            string input = Microsoft.VisualBasic.Interaction.InputBox("Ingrese el peso", "Datos", "1", 500, 300);
        }

        private void textBox1_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!String.IsNullOrEmpty(txtpago.Text) && total > 0)
                {
                    try
                    {
                        decimal pago = Convert.ToInt32(txtpago.Text);
                        if (total <= pago)
                        {
                            decimal vuelto = pago - total;
                            lblVuelto.Text = vuelto.ToString();
                        }
                        else
                        {
                            MessageBox.Show("Monto insuficiente");
                        }

                    }
                    catch (Exception)
                    {

                        MessageBox.Show("Ingreso mal los datos");
                    }
                }
                else
                {
                    MessageBox.Show("Ingrese un monto o producto");
                }
            }
        }

        public int  facturacion()
        {
            Facturacion f = new Facturacion()
            {
                dia = DateTime.Today.Day,
                mes = DateTime.Today.Month,
                ano = DateTime.Today.Year,
                total = total,
                idUsuario = 2,
                codigofactura = ""
                
        };
            if (comboBox1.SelectedItem.Equals("CONTADO"))
            {
                f.tipo = "contado";
                f.bauche = "";
            }
            else
            {
                f.tipo = "tarjeta";
                f.bauche = txtBauche.Text;
            }

            return fbol.registrardetallefactura(f);



        }

        private void btnCobrar_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(lblVuelto.Text))
            {
                RestarInventario();
                int iddef = facturacion();
                productodetalle(iddef);
                limipiar();
            }
            else
            {
                MessageBox.Show("Termine de llenar los datos");
            }

        }

        public void productodetalle(int iddf)
        {

            foreach (Producto pro in productos)
            {
                fbol.insertProductoDetalle(iddf, pro.id, pro.cantidad);
            }
        }
        public void RestarInventario()
        {
            foreach (Producto pro in productos)
            {
                ;
                pbol.RestarInventario(pro);
            }
        }

        private void txtpago_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar == (char)Keys.Back)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.Equals("CONTADO"))
            {
                pbauche.Visible = false;
            }
            else
            {
                pbauche.Visible = true;
            }
        }

        public void limipiar()
        {

            for (int i = 0; i < productos.Count; i++)
            {
                if (i % 5 == 0)
                {
                    productos.RemoveAt(i);
                }
                    
            }

            dgProducto.DataSource = null;
            lblt.Text = "";
            lblVuelto.Text = "";
            txtBauche.Text = "";
            txtpago.Text = "";
            txtCode.Text = "";
            total = 0;
            
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limipiar();
        }
    }
}

