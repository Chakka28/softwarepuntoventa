﻿using PuntoVentaBOL;
using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PuntoVenta
{

    public partial class FrmCategoria : Form
    {
        private Categoria cat;
        private CategoryBOL cbol;

        public FrmCategoria()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                cat.nombre = txtNombre.Text.Trim();
                cat.descripcion = txtDes.Text.Trim();
                cbol.insert(cat);
                MessageBox.Show("Datos agregados");
                limpieza();
                


            }
            catch (Exception)
            {

                throw;
            }
        }

        public void limpieza()
        {
            txtNombre.Text = "";
            txtDes.Text = "";
            cat.id = 0;


        }

        private void FrmCategoria_Load(object sender, EventArgs e)
        {
            cat = new Categoria();
            cat.id = 0;
            cbol = new CategoryBOL();
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            try
            {
                cat = (Categoria)ggCategorias.SelectedRows[0].DataBoundItem;
                llenarModificacion();
                
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        public void llenarModificacion()
        {
            txtNombre.Text = cat.nombre;
            txtDes.Text = cat.descripcion;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ggCategorias.DataSource = cbol.LoadProvider("");
            
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                cat = (Categoria)ggCategorias.SelectedRows[0].DataBoundItem;

                DialogResult result = MessageBox.Show("¿Desea Eliminar este proveedor?",
             "Mensaje del sistema", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {

                    cbol.Delete(cat);
                    limpieza();
                }
                else
                {

                }
            }
            catch (Exception)
            {

                MessageBox.Show("Seleccione una columna");
            }
        }

    }
}