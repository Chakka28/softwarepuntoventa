﻿using PuntoVentaDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using PuntoVentaEntities;
using PuntoVentaBOL;

namespace PuntoVenta
{
    public partial class Login : Form
    {
        private UserBOL ubol;

        public Login()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnaccept_Click(object sender, EventArgs e)
        {

            try
            {
                User u = new User
                {
                    userid = txtuser.Text.Trim(),
                    pass = txtpass.Text.Trim(),
                    
                };
                u = ubol.Login(u);
               
                if (u.id > 0 & u.Rol.Equals("cajero"))
                {
                    MessageBox.Show(u.Rol);
                    MenuPrincipal mp = new MenuPrincipal {
                        Usuario = u
                    };
                    mp.Show(this);
                    Hide();
                
                }
                else
                {
                    MessageBox.Show(u.Rol);
                    MenuPrincipal mp = new MenuPrincipal
                    {
                        Usuario = u
                    };
                    mp.Show(this);
                    Hide();
                }
            }
            catch(Exception ex)
            {
                ///Se deberia manejar mejor los errores y escribirlos en un archivo txt o xml
                ///para tener un tipo de registro
                MessageBox.Show(ex.Message);
            }
           
        }

        

        private void Login_Load_1(object sender, EventArgs e)
        {
            ubol = new UserBOL();
        }
    }
}
