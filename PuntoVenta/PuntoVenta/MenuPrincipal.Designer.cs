﻿namespace PuntoVenta
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuPrincipal));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btbcierre = new System.Windows.Forms.Button();
            this.btbusuarios = new System.Windows.Forms.Button();
            this.btbinventario = new System.Windows.Forms.Button();
            this.btbcaja = new System.Windows.Forms.Button();
            this.usernamelbl = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btbcierre);
            this.panel1.Controls.Add(this.btbusuarios);
            this.panel1.Controls.Add(this.btbinventario);
            this.panel1.Controls.Add(this.btbcaja);
            this.panel1.Location = new System.Drawing.Point(17, 30);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(623, 282);
            this.panel1.TabIndex = 0;
            // 
            // btbcierre
            // 
            this.btbcierre.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbcierre.Image = ((System.Drawing.Image)(resources.GetObject("btbcierre.Image")));
            this.btbcierre.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btbcierre.Location = new System.Drawing.Point(17, 174);
            this.btbcierre.Margin = new System.Windows.Forms.Padding(4);
            this.btbcierre.Name = "btbcierre";
            this.btbcierre.Size = new System.Drawing.Size(280, 68);
            this.btbcierre.TabIndex = 3;
            this.btbcierre.Text = "    Cierre de caja";
            this.btbcierre.UseVisualStyleBackColor = true;
            this.btbcierre.Click += new System.EventHandler(this.btbcierre_Click);
            // 
            // btbusuarios
            // 
            this.btbusuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbusuarios.Image = ((System.Drawing.Image)(resources.GetObject("btbusuarios.Image")));
            this.btbusuarios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btbusuarios.Location = new System.Drawing.Point(317, 98);
            this.btbusuarios.Margin = new System.Windows.Forms.Padding(4);
            this.btbusuarios.Name = "btbusuarios";
            this.btbusuarios.Size = new System.Drawing.Size(280, 68);
            this.btbusuarios.TabIndex = 2;
            this.btbusuarios.Text = "Usuarios";
            this.btbusuarios.UseVisualStyleBackColor = true;
            this.btbusuarios.Click += new System.EventHandler(this.btbusuarios_Click);
            // 
            // btbinventario
            // 
            this.btbinventario.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btbinventario.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbinventario.Image = ((System.Drawing.Image)(resources.GetObject("btbinventario.Image")));
            this.btbinventario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btbinventario.Location = new System.Drawing.Point(17, 98);
            this.btbinventario.Margin = new System.Windows.Forms.Padding(4);
            this.btbinventario.Name = "btbinventario";
            this.btbinventario.Size = new System.Drawing.Size(280, 68);
            this.btbinventario.TabIndex = 1;
            this.btbinventario.Text = "Inventario";
            this.btbinventario.UseVisualStyleBackColor = true;
            this.btbinventario.Click += new System.EventHandler(this.btbinventario_Click);
            // 
            // btbcaja
            // 
            this.btbcaja.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btbcaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbcaja.Image = ((System.Drawing.Image)(resources.GetObject("btbcaja.Image")));
            this.btbcaja.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btbcaja.Location = new System.Drawing.Point(17, 22);
            this.btbcaja.Margin = new System.Windows.Forms.Padding(4);
            this.btbcaja.Name = "btbcaja";
            this.btbcaja.Size = new System.Drawing.Size(280, 68);
            this.btbcaja.TabIndex = 0;
            this.btbcaja.Text = "Caja";
            this.btbcaja.UseVisualStyleBackColor = false;
            this.btbcaja.Click += new System.EventHandler(this.btbcaja_Click);
            // 
            // usernamelbl
            // 
            this.usernamelbl.AutoSize = true;
            this.usernamelbl.Location = new System.Drawing.Point(476, 9);
            this.usernamelbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.usernamelbl.Name = "usernamelbl";
            this.usernamelbl.Size = new System.Drawing.Size(160, 17);
            this.usernamelbl.TabIndex = 1;
            this.usernamelbl.Text = "Nombre de usuario aqui";
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 328);
            this.Controls.Add(this.usernamelbl);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MenuPrincipal";
            this.Text = "Menu principal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.MenuPrincipal_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btbcierre;
        private System.Windows.Forms.Button btbusuarios;
        private System.Windows.Forms.Button btbinventario;
        private System.Windows.Forms.Button btbcaja;
        private System.Windows.Forms.Label usernamelbl;
    }
}