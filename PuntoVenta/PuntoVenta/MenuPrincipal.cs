﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PuntoVentaEntities;

namespace PuntoVenta
{
    public partial class MenuPrincipal : Form
    {

        public User Usuario { get; set; }

        public MenuPrincipal()
        {
            InitializeComponent();
            this.CenterToScreen();

        }

        /// <summary>
        /// Verifica el tipo de usuario que inicia sesión
        /// y de esta manera decide que opciones se pueden o no
        /// habilitar
        /// </summary>
        /// <param name="u">Objeto usuario</param>
        private void VerificarTipoUser(User u)
        {
            if (u.Rol.Equals("cajero"))
            {
                usernamelbl.Text = Usuario.name;
                btbusuarios.Visible = false;
            }
            else {
             
                usernamelbl.Text = Usuario.name;

            }
        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {
            VerificarTipoUser(Usuario);
        }

        public static implicit operator MainMenu(MenuPrincipal v)
        {
            throw new NotImplementedException();
        }

        private void btbcaja_Click(object sender, EventArgs e)
        {
            FrmCaja caja = new FrmCaja
            {
                usuario = Usuario

            };
            caja.Show(this);
            Hide();
        }

        private void btbinventario_Click(object sender, EventArgs e)
        {
            FrmInventario frminven = new FrmInventario
            {
                Usuario = Usuario
            };
            frminven.Show(this);
            Hide();
        }

        private void btbcierre_Click(object sender, EventArgs e)
        {
            
        }   

        private void btbusuarios_Click(object sender, EventArgs e)
        {
            FrmAdminUsuarios frmUsers = new FrmAdminUsuarios
            {
                //Usuario = Usuario
            };
            frmUsers.Show(this);
            Hide();
        }

        /// <summary>
        /// Este evento se encarga de cerrar el formulario y volver al de login.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Desea salir del sistema?",
                 "Mensaje del sistema", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                if (Owner != null)
                {
                    Owner.Show();
                }

            }
            else {
                e.Cancel = true;
            }
        }
    }
}
