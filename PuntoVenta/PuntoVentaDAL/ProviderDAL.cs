﻿using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaDAL
{
    public class ProviderDAL
    {
        private SqlCommand cmd;
        private SqlDataReader dr;


        /// <summary>
        /// Registra proveedor 
        /// </summary>
        /// <param name="provider"></param>
        public void Insert(Provider provider)
        {
            Connection con = new Connection();
            con.open();

            try
            {
                cmd = new SqlCommand("insert into providers(company, telephone, active) values ('" + provider.company + "','" + provider.telephone + "', 1)", con.con);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                Console.WriteLine("Error a registrar");
            }
            con.Close();

        }

        public List<Provider> LoadProvider(string filtro)
        {
            Connection con = new Connection();
            List<Provider> provider = new List<Provider>();
            con.open();

            try
            {
                String sql = "select id,company,telephone from providers where active = 1";

                if (!String.IsNullOrEmpty(filtro))
                {
                    sql = "select id,company,telephone from providers where lower(company) like lower(@par) and active = 1";
                }
                cmd = new SqlCommand(sql, con.con);

                if (!String.IsNullOrEmpty(filtro))
                {
                    cmd.Parameters.AddWithValue("@par", filtro + "%");
                }
                dr = cmd.ExecuteReader();
                
                while (dr.Read())
                {
                    Provider p = new Provider
                    {
                        id = dr.GetInt32(0),
                        company = dr.GetString(1),
                        telephone = dr.GetString(2)
                    };

                    provider.Add(p);
                }
                con.Close();
                return provider;
                

            }
            catch (Exception)
            {

                Console.WriteLine("Error ");
            }
            return provider;
        }

        internal Provider GetProvedor(int v)
        {
            throw new NotImplementedException();
        }

        public void Delete(Provider p)
        {
            Connection con = new Connection();
            con.open();

            try
            {
                String sql = "update providers set active = 0 where id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@id", p.id);
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                Console.WriteLine("Error ");
            }
            con.Close();
        }

        public void Modify(Provider p)
        {
            Connection con = new Connection();
            con.open();

            String sql = "update providers set company = @com , telephone= @cel where id = @id";
            cmd = new SqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@com", p.company);
            cmd.Parameters.AddWithValue("@cel", p.telephone);
            cmd.Parameters.AddWithValue("@id", p.id);
            cmd.ExecuteNonQuery();

            con.Close();

        }

        /// <summary>
        /// Carga todos los datos de todos los proveedores que existen 
        /// en la base de datos
        /// </summary>
        /// <returns>List de proveedores</returns>
        public List<Provider> CargarTodos()
        {
            Connection con = new Connection();
            con.open();
            List<Provider> proveedores = new List<Provider>();
            try
            {
                cmd = new SqlCommand("select * from providers where active = 1", con.con);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    proveedores.Add(CrearProveedor(dr));
                }
                con.Close();

            }
            catch
            {
                Console.WriteLine("Error ");
            }
            con.Close();
            return proveedores;
        }

        /// <summary>
        /// Da formato al objeto que viene por parametro
        /// </summary>
        /// <param name="dr">SQlDataReader trae datos de proveedores</param>
        /// <returns>Objeto provider con todos sus datos</returns>
        private Provider CrearProveedor(SqlDataReader dr)
        {
            Provider p = new Provider();
            p.id = dr.GetInt32(0);
            p.company = dr.GetString(1);
            p.telephone = dr.GetString(2);
            return p;
        }
    }
}
