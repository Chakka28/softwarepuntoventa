﻿using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace PuntoVentaDAL
{
    public class UsersDAL
    {
        private SqlCommand cmd;
        private SqlDataReader dr;

        /// <summary>
        /// This method registry users 
        /// </summary>
        /// <param name="user">user</param>
        public bool Insert(User user)
        {
            Connection con = new Connection();
            con.open();

            try
            {
                cmd = new SqlCommand("insert into users(nombre,cedula,iduser,pass, rol, active) values ('" + user.name + "','" + user.cedula +"','"+ user.userid +
                "','" + user.pass + "','" + user.Rol + "'," + "1)", con.con);
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch
            {
                throw new Exception("Error al registrar usuario");
            }
            
            

        }

        /// <summary>
        /// Este método se usa para cargar los usuarios que estan
        /// activos
        /// </summary>
        /// <returns>Lista con los usuarios que estan activos</returns>
        public List<User> LeerUsuarios()
        {
            Connection con = new Connection();
            con.open();
            List<User> usuarios = new List<User>();
            try
            {
                cmd = new SqlCommand("select * from users where active = 1", con.con);
                dr = cmd.ExecuteReader();
                while(dr.Read())
                {
                    usuarios.Add(CrearUsuario(dr));
                }
                con.Close();

            }
            catch
            {
                Console.WriteLine("Error ");
            }
            con.Close();
            return usuarios;

        }
         
        /// <summary>
        /// Este método permite crear los objetos User
        /// con los datos que vienen desde la base de datos.
        /// </summary>
        /// <param name="dr"></param>
        /// <returns>Objeto User para pasarlo</returns>
        private User CrearUsuario(SqlDataReader dr)
        {
            User u = new User();
            u.id = dr.GetInt32(0);
            u.name = dr.GetString(1);
            u.cedula = dr.GetString(2);
            u.userid = dr.GetString(3);
            u.Rol = dr.GetString(5);
            u.active = true;
            return u;
        }


        /// <summary>
        /// este metodo busca el login en la base de datos
        /// </summary>
        /// <param name="user">los datos del logeado</param>
        /// <returns>un user con todos los datos</returns>
        public User Login(User user)
        {
            Connection con = new Connection();
            con.open();

            try
            {
                cmd = new SqlCommand("select id,nombre,cedula,rol from users where iduser = '" + user.userid + "' and pass= '" + user.pass + "'", con.con);
                dr = cmd.ExecuteReader();
                if(dr.Read())
                {
                    user.id = dr.GetInt32(0);
                    user.name = dr.GetString(1);
                    user.cedula = dr.GetString(2);
                    user.Rol = dr.GetString(3);
                }
               
                con.Close();
                
            }
            catch
            {
                Console.WriteLine("Error ");
            }
            con.Close();
            return user;

            


        }

        /// <summary>
        /// Este método es el encargo de desactivar a los usuarios 
        /// en la base de datos
        /// </summary>
        /// <param name="euser">Objeto user</param>
        /// <returns>True si el usuario se desactivo con éxito si no envía un mensaje de error</returns>
        public bool EliminarUsuario(User euser)
        {
            Connection con = new Connection();
            con.open();
            try
            {
                String sql = "UPDATE users SET active = 0 WHERE id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@id", euser.id);
                return cmd.ExecuteNonQuery() > 0;

            }
            catch (Exception)
            {
                throw new Exception("Error a la de eliminar el usuario");
            }
        }


        /// <summary>
        /// Este método funciona para la actualización de datos 
        /// de los usuarios activos.
        /// </summary>
        /// <param name="euser">Objeto user</param>
        /// <returns>true si la actualización de datos fue exitosa</returns>
        public bool ModificarUsuario(User euser)
        {
            Connection con = new Connection();
            con.open();
            try
            {
                String sql = "UPDATE users SET nombre = @nombre, cedula = @cedula, iduser = @iduser, rol = @rol WHERE id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@nombre", euser.name);
                cmd.Parameters.AddWithValue("@cedula", euser.cedula);
                cmd.Parameters.AddWithValue("@iduser", euser.userid);
                cmd.Parameters.AddWithValue("@rol", euser.Rol);
                cmd.Parameters.AddWithValue("@id", euser.id);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception )
            {
                
               throw new Exception("Error a la hora de realizar la actualización de datos");
            }
            

        }
    }
}
