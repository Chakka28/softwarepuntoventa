﻿using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaDAL
{
    public class ProductoDAL
    {
        private SqlCommand cmd;
        private SqlDataReader dr;
        private ProviderDAL pdal = new ProviderDAL();
        private CategoriaDAL cdal = new CategoriaDAL();

        public Producto CargarProducto(string codigo)
        {
            Producto p = new Producto();
            Connection con = new Connection();
            con.open();

            try
            {
                String sql = "select id,idcategoria,idprovedor,codigobarra,cantidad,precio,descripcion from productos where codigobarra ='" + codigo + "'";
                cmd = new SqlCommand(sql, con.con);
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {

                    ///Se puede cambiar por el método crear producto
                    p.id = dr.GetInt32(0);
                    p.codigo = dr.GetString(3);
                    p.precio = dr.GetDecimal(5);
                    p.Descripcion = dr.GetString(6);
                    p.cantidad = 1;
                    p.categoria = new Categoria();
                    p.categoria.id = dr.GetInt32(1);
                    p.provedor = new Provider();
                    p.provedor.id = dr.GetInt32(2);
                }

                con.Close();
                return p;
            }
            catch (Exception)
            {

                Console.WriteLine("Error ");
            }
            con.Close();
            return p;
        }

        public void RestarInventario(Producto p)
        {
            Connection con = new Connection();
            con.open();
            try
            {
                String sql = "update productos set cantidad = cantidad-@cant where id = @id ";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@cant", p.cantidad);
                cmd.Parameters.AddWithValue("@id", p.id);
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {

                throw new Exception("Error a la hora de realizar la actualización de datos");
            }
            con.Close();
            
        }

        private Producto CargarProductoSolo(SqlDataReader dr)
        {
            Producto p = new Producto();
            p.id = dr.GetInt32(0);
            p.categoria =   cdal.GetCategoria(dr.GetInt32(1));
            p.provedor = pdal.GetProvedor(dr.GetInt32(2));
            p.codigo = dr.GetString(3);
            p.cantidad = dr.GetInt32(4);
            p.precio = dr.GetDecimal(5);
            p.Descripcion = dr.GetString(6);

            return p;
        }

        /// <summary>
        /// Metodo que carga todos los productos de la base de datos
        ///y los agrega a una lista
        /// </summary>
        /// <returns>List con objetos productos para ser mostrada</returns>
        public List<Producto> CargarTodosProductos()
        {
            List<Producto> producs = new List<Producto>();
            List<Provider> proveedores = pdal.CargarTodos();
            Connection con = new Connection();
            con.open();

            try
            {
                String sql = "SELECT * FROM productos  INNER JOIN categorias ON productos.idcategoria = categorias.id INNER JOIN providers ON productos.idprovedor = providers.id";
                cmd = new SqlCommand(sql, con.con);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    producs.Add(CrearProducto(dr));

                }

                con.Close();
                
            }
            catch (Exception)
            {

                //throw new Exception("No se pudo contactar con el servidor");
            }
            con.Close();
            return producs;

        }

        /// <summary>
        /// Este metodo se encarga de crear el objeto producto
        /// </summary>
        /// <param name="dr"> SQLDataReader</param>
        /// <returns>Objeto producto con sus respectivos datos</returns>
        private Producto CrearProducto(SqlDataReader dr)
        {
            Producto p = new Producto();
            p.id = dr.GetInt32(0);
            p.categoria = new Categoria();
            p.categoria.id = dr.GetInt32(1);
            p.provedor = new Provider();
            p.provedor.id = dr.GetInt32(2);
            p.codigo = dr.GetString(3);
            p.cantidad = dr.GetInt32(4);
            p.precio = dr.GetDecimal(5);
            p.Descripcion = dr.GetString(6);
            p.categoria.nombre = dr.GetString(8);
            p.categoria.descripcion = dr.GetString(9);
            p.categoria.activo = true;
            p.provedor.company = dr.GetString(12);
            p.provedor.telephone = dr.GetString(13);
            p.provedor.activo = true;

            return p;
        }
    }
}
