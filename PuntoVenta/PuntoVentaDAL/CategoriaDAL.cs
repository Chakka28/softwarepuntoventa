﻿using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaDAL
{
    public class CategoriaDAL
    {
        private SqlCommand cmd;
        private SqlDataReader dr;

        public List<Categoria> LoadCategoria(string filtro)
        {
            Connection con = new Connection();
            List<Categoria> categorias = new List<Categoria>();
            con.open();

            try
            {
                String sql = "select id,nombre,descripcion from categorias where active = 1";

                if (!String.IsNullOrEmpty(filtro))
                {
                    sql = "select id,nombre,descripcion from categorias where lower(nombre) like lower(@par) and active = 1";
                }
                cmd = new SqlCommand(sql, con.con);

                if (!String.IsNullOrEmpty(filtro))
                {
                    cmd.Parameters.AddWithValue("@par", filtro + "%");
                }
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Categoria p = new Categoria
                    {
                        id = dr.GetInt32(0),
                        nombre = dr.GetString(1),
                        descripcion = dr.GetString(2)
                    };

                    categorias.Add(p);
                }
                con.Close();
                return categorias;


            }
            catch (Exception)
            {

                Console.WriteLine("Error ");
            }
            return categorias;
        }

        internal Categoria GetCategoria(int v)
        {
            throw new NotImplementedException();
        }

        public void Insert(Categoria categoria)
        {
            Connection con = new Connection();
            con.open();

            try
            {
                cmd = new SqlCommand("insert into categorias(nombre, descripcion, active) values ('" + categoria.nombre + "','" + categoria.descripcion + "', 1)", con.con);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                Console.WriteLine("Error a registrar");
            }
            con.Close();

        }

        public void Modify(Categoria c)
        {
            Connection con = new Connection();
            con.open();

            String sql = "update categorias set nombre = @com , descripcion= @cel where id = @id";
            cmd = new SqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@com", c.nombre);
            cmd.Parameters.AddWithValue("@cel", c.descripcion);
            cmd.Parameters.AddWithValue("@id", c.id);
            cmd.ExecuteNonQuery();

            con.Close();

        }

        public void Delete(Categoria c)
        {
            Connection con = new Connection();
            con.open();

            try
            {
                String sql = "update categorias set active = 0 where id = @id";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@id", c.id);
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                Console.WriteLine("Error ");
            }
            con.Close();
        }

        /// <summary>
        /// Carga todas las categorias en la base de datos 
        /// </summary>
        /// <returns>List de objetos categoria para luego mostrar</returns>
        public List<Categoria> CargarTodas()
        {
            Connection con = new Connection();
            con.open();
            List<Categoria> categorias = new List<Categoria>();
            try
            {
                cmd = new SqlCommand("select * from categorias where active = 1", con.con);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    categorias.Add(CrearCategoria(dr));
                }
                con.Close();

            }
            catch
            {
                Console.WriteLine("Error ");
            }
            con.Close();
            return categorias;
        }

        /// <summary>
        /// Da formato al objeto que viene por parametro
        /// </summary>
        /// <param name="dr">SQlDataReader trae datos de las categorias</param>
        /// <returns>Objeto Categoria con todos sus datos</returns>
        private Categoria CrearCategoria(SqlDataReader dr)
        {
            Categoria c = new Categoria();
            c.id = dr.GetInt32(0);
            c.nombre = dr.GetString(1);
            c.descripcion = dr.GetString(2);
            return c;
        }


    }
}
