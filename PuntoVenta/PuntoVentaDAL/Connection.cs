﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace PuntoVentaDAL
{
    public class Connection
    {
        //Sebas pc = DESKTOP-0P9GJQO\\SQLEXPRESS
        //string dataBD = "Data Source = DESKTOP-0P9GJQO\\SQLEXPRESS;Initial Catalog=puntoventa;Integrated Security =True";
        public SqlConnection con = new SqlConnection("Data Source =  LAPTOP-F9Q2F3TA\\SQLEXPRESS;Initial Catalog=puntoventa;Integrated Security =True");
        //public SqlConnection con = new SqlConnection("Data Source = DESKTOP-0P9GJQO\\SQLEXPRESS;Initial Catalog=puntoventa;Integrated Security =True");

        public Connection()
        {

        }

        public SqlConnection cone()
        {
            return con;
        }

        /// <summary>
        /// Open de data base 
        /// </summary>
        public void open()
        {
            
            try
            {
                con.Open();
                
            }
            catch
            {
                Console.WriteLine("Error al abrir la base de datos");
            }
        }

        /// <summary>
        /// Close de data base 
        /// </summary>
        public void Close()
        {
            con.Close();
        }
    }
}
