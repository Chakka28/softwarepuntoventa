﻿using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaDAL
{
    public class FacturacionDAL
    {
        private SqlCommand cmd;
        private SqlDataReader dr;

        public int insertardetalle(Facturacion f)
        {
            Connection con = new Connection();
            con.open();
            //VALUES (@dia,@mes,@ano,@tipo,@bauche,@total,@cod,@idu) 
            try
            {
                String sql = "declare @a int " +
                    "INSERT INTO [dbo].[detallefactura]([dia],[mes],[ano],[tipo],[baouche],[total],[codigofactura],[idUsuario]) " +
                    " VALUES (@dia,@mes,@ano,@tipo,@bauche,@total,@cod,@idu) " +
                    " select @a = scope_identity() " +
                    " select @a";
                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@dia", f.dia);
                cmd.Parameters.AddWithValue("@mes", f.mes);
                cmd.Parameters.AddWithValue("@ano", f.ano);
                cmd.Parameters.AddWithValue("@tipo", f.tipo);
                cmd.Parameters.AddWithValue("@bauche", f.bauche);
                cmd.Parameters.AddWithValue("@total", f.total);
                cmd.Parameters.AddWithValue("@cod", f.codigofactura);
                cmd.Parameters.AddWithValue("@idu", f.idUsuario);
                Console.WriteLine("por el momento si");
                dr = cmd.ExecuteReader();
                
                if (dr.Read())
                {
                    return dr.GetInt32(0);
                }

                
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
            }
            return 0;
        }

        public void insertProductoDetalle(int idf, int idp, int cant)
        {
            Connection con = new Connection();
            con.open();
            try
            {
                String sql = "insert into detallefactura_producto(iddetallefactura,idproducto,cantidad) " +
                    " values(@idf, @idp, @cant)";

                cmd = new SqlCommand(sql, con.con);
                cmd.Parameters.AddWithValue("@idf", idf);
                cmd.Parameters.AddWithValue("@idp", idp);
                cmd.Parameters.AddWithValue("@cant", cant);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
            }
        }
    }
}
