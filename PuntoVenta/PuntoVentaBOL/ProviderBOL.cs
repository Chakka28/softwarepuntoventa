﻿using PuntoVentaDAL;
using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaBOL
{
    public class ProviderBOL
    {
        ProviderDAL pdal;

        public ProviderBOL()
        {
            pdal = new ProviderDAL();
        }


        public void insert(Provider provider)
        {
            if (String.IsNullOrEmpty(provider.company))
            {
                throw new Exception("Datos requeridos");

            }
            if (provider.id > 0)
            {
                pdal.Modify(provider);
            }
            else
            {
                pdal.Insert(provider);
            }

            
        }

        public List<Provider> LoadProvider(String filtro)
        {
            return pdal.LoadProvider(filtro);
        }

        public void Delete(Provider p)
        {
            pdal.Delete(p);
        }


        /// <summary>
        /// Este metodo carga todos los proveedores registrados 
        /// para ser mostrados luego.
        /// </summary>
        /// <returns>Lista con el objeto provedores para luego ser mostrado</returns>
        public List<Provider> CargarTodosProveedores()
        {
            return pdal.CargarTodos();
        }
    }
}

