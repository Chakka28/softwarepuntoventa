﻿using PuntoVentaDAL;
using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaBOL
{
    public class FacturacionBOL
    {
        private FacturacionDAL fdal;

        public FacturacionBOL()
        {
            fdal = new FacturacionDAL();
        }

        public int registrardetallefactura(Facturacion f)
        {
            return fdal.insertardetalle(f);
        }

        public void insertProductoDetalle(int idf, int idp, int cant)
        {
            fdal.insertProductoDetalle(idf, idp, cant);
        }


    }
}
