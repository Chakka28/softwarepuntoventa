﻿using PuntoVentaDAL;
using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaBOL
{
    public class CategoryBOL
    {
        private CategoriaDAL cdal;

        public CategoryBOL()
        {
            cdal = new CategoriaDAL();
        }

        public List<Categoria> LoadProvider(String filtro)
        {
            return cdal.LoadCategoria(filtro);
        }


        public void insert(Categoria categoria)
        {
            if (String.IsNullOrEmpty(categoria.nombre))
            {
                throw new Exception("Datos requeridos");

            }
            if (categoria.id > 0)
            {
                Console.WriteLine("AQUI ES ");
                cdal.Modify(categoria);
            }
            else
            {
                cdal.Insert(categoria);
            }
        }

        public void Delete(Categoria c)
        {
            cdal.Delete(c);
        }
        
        /// <summary>
        /// Se encarga de retornar una lista de categorías en
        /// forma de objetos para luego ser mostradas
        /// </summary>
        /// <returns>List de categorías para mostrarlas en el inventario</returns>
        public List<Categoria> CargarTodas()
        {
            return cdal.CargarTodas();
        }
    }

}
