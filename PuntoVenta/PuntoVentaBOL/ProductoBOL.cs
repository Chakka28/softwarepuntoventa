﻿using PuntoVentaDAL;
using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaBOL
{
    public class ProductoBOL
    {
        private ProductoDAL pdal;

        public ProductoBOL()
        {
            this.pdal = new ProductoDAL();
        }

        public Producto cargarProducto(String code)
        {
            return pdal.CargarProducto(code);
        }

        /// <summary>
        /// Este método carga todos los productos
        /// </summary>
        /// <returns>List con todos los objetos de productos</returns>
        public List<Producto> CargarProductos()
        {
            return pdal.CargarTodosProductos();
        }

        public void RestarInventario(Producto p)
        {
            pdal.RestarInventario(p);
        }
    }
}
