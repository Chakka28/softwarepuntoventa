﻿using PuntoVentaDAL;
using PuntoVentaEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaBOL
{
    public class UserBOL
    {
        UsersDAL udal;


        public UserBOL()
        {
            this.udal = new UsersDAL();
        }

        /// <summary>
        /// verifica los datos que esten llenos y 
        /// llma al dal para verificar el logeado
        /// </summary>
        /// <param name="user">usuario</param>
        /// <returns>user</returns>
        public User Login(User user)
        {
            if (String.IsNullOrEmpty(user.userid) || String.IsNullOrEmpty(user.pass)){
                throw new Exception("Datos requeridos");
                
            }
            else
            {
                return udal.Login(user);
            }
        }

        /// <summary>
        /// Este método carga todos los usuarios activos 
        /// registrados en la base de datos
        /// </summary>
        /// <returns>Lista con los usuarios activos dentro del sistema para mostrar al administrador</returns>
        public List<User> CargarDatos()
        {
            return udal.LeerUsuarios(); 
        }


        /// <summary>
        /// Este método se encarga de realizar los inserts de los nuevos 
        /// usuarios en la base de datos, a su vez se encarga de validar que
        /// todos los datos esten completos.
        /// </summary>
        /// <param name="euser"></param>
        /// <returns>True si el insert se realizó con éxito, de lo contrario se false o un error</returns>
        public bool InsertarDatos(User euser, string repass)
        {
            if(String.IsNullOrEmpty(euser.name))
            {
                throw new Exception("Nombre y apellidos requeridos");
            }
            if (String.IsNullOrEmpty(euser.cedula))
            {
                throw new Exception("Se requiere un número de cédula");
            }
            if (String.IsNullOrEmpty(euser.userid))
            {
                throw new Exception("Se requiere un nombre de usuario");
            }
            if (String.IsNullOrEmpty(euser.pass))
            {
                throw new Exception("Se debe digitar una contraseña");
            }
            if (String.IsNullOrEmpty(repass))
            {
                throw new Exception("Debe de confirmar contraseña");
            }
            if (!euser.pass.Equals(repass))
            {
                throw new Exception("Las contraseñas deben de concidir, por favor vuelva a escribirlas");
            }
            if (String.IsNullOrEmpty(euser.Rol))
            {
                throw new Exception("Debe de seleccionar un puesto para el nuevo usuario");
            }

            return udal.Insert(euser);
        }

        /// <summary>
        /// Este método se encarga de desactivar los usuarios
        /// de la base de datos
        /// </summary>
        /// <param name="euser"></param>
        /// <returns></returns>
        public bool EliminarUsuario(User euser)
        {
            return udal.EliminarUsuario(euser);
        }

        /// <summary>
        /// Este metodo se encarga de verificar que todos lo datos se encuentre correctos
        /// y además realiza en update en la base de datos
        /// </summary>
        /// <param name="euser">Objeto usuario</param>
        /// <returns>true si la actualización de datos fue exitosa</returns>
        public bool ModificarDatos(User euser)
        {
            if (String.IsNullOrEmpty(euser.name))
            {
                throw new Exception("Nombre y apellidos requeridos");
            }
            if (String.IsNullOrEmpty(euser.cedula))
            {
                throw new Exception("Se requiere un número de cédula");
            }
            if (String.IsNullOrEmpty(euser.userid))
            {
                throw new Exception("Se requiere un nombre de usuario");
            }
            if (String.IsNullOrEmpty(euser.Rol))
            {
                throw new Exception("Debe de seleccionar un puesto para el nuevo usuario");
            }

            return udal.ModificarUsuario(euser);
        }
    }
}

