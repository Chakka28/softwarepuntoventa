﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaEntities
{
    public class Facturacion
    {
        public int id { get; set; }
        public int dia { get; set; }
        public int mes { get; set; }
        public int ano { get; set; }
        public decimal total { get; set; }
        public string tipo { get; set; }
        public string bauche { get; set; }
        public string codigofactura{ get; set; }
        public int idUsuario { get; set; }
    }
}
