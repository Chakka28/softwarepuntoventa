﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaEntities
{
    public class Producto
    {
        public int id { get; set; }
        public Categoria categoria { get; set; } //cambiar por el objeto categoria directamente
        public Provider provedor { get; set; }  //cambiar por el objeto provedor 
        public string codigo { get; set; }
        public int cantidad { get; set; }
        public decimal precio { get; set; }
        public string Descripcion { get; set; }
    }
}
