﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaEntities
{
    public class Provider
    {
        public int id { get; set; }
        public string company { get; set; }
        public string telephone { get; set; }
        public Boolean activo { get; set; }


        public override string ToString()
        {
            return this.company.ToString();
        }
    }


    
}
