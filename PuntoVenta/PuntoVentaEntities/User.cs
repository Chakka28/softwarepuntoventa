﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoVentaEntities
{
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string cedula { get; set; }
        public string userid { get; set; }
        public string pass { get; set; }
        public string Rol { get; set; }
        public bool active { get; set; }
    }
}
